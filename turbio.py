#!/usr/bin/env python

"""Turbio - An ipmitool wrapper

Usage:
    turbio.py [-c <conf>] <hostlist> <command>...
    turbio.py [-c <conf>] <group> <command>...
    turbio.py --help

Options:
  -h --help                 This screen
  -c <conf>, --conf=<conf>  Path to config file [default: /etc/turbio.yml]

"""

from __future__ import print_function
from hostlist import expand_hostlist
from docopt import docopt
import os
import sys
import subprocess
import yaml


def main(opts):
    conffile = opts['--conf']
    hostlist = opts['<hostlist>']
    command = opts['<command>']

    conf = read_host_configuration(conffile)
    reverse = generate_reverse_mapping(conf)

    if hostlist in conf and hostlist != 'default':
        hostlist = conf[hostlist]['hostlist']
    launch_ipmitool(conf, reverse, hostlist, command)


def read_host_configuration(conffile):
    with open(conffile, 'r') as ymlconf:
        conf = yaml.load(ymlconf)
    process_host_configuration(conf)
    return conf


def process_host_configuration(conf):
    if 'default' not in conf:
        raise SyntaxError('no default group defined in configuration')
    for group in conf:
        if group == 'default':
            validate_group(conf, group, ['user', 'password', 'interface'])
        else:
            validate_group(conf, group, ['hostlist'])


def validate_group(conf, group, reqs):
    for req in reqs:
        if req not in conf[group]:
            raise SyntaxError('no {} defined in {} group'.format(req, group))


def generate_reverse_mapping(conf):
    reverse = dict()
    for group in conf:
        if group != 'default':
            for host in expand_hostlist(conf[group]['hostlist']):
                reverse[host] = group
    return reverse


def launch_ipmitool(conf, reverse, hostlist, command):
    for host in expand_hostlist(hostlist):
        group = conf[reverse.get(host, 'default')]
        user = group.get('user', conf['default']['user'])
        password = group.get('password', conf['default']['password'])
        interface = group.get('interface', conf['default']['interface'])

        os.environ['IPMITOOL_PASSWORD'] = password
        cmd = ['ipmitool', '-I', interface, '-H', host, '-U', user, '-E']
        cmd.extend(command)
        print('{}: '.format(host), end='')
        sys.stdout.flush()
        subprocess.call(cmd)


if __name__ == '__main__':
    try:
        opts = docopt(__doc__)
        main(opts)
    except KeyboardInterrupt:
        print('Caught keyboard interrupt.')
