# turbio
An ipmitool wrapper that supports per-node configuration and hostlists

```
Usage:
    turbio.py [-c <conf>] <hostlist> <command>...
    turbio.py [-c <conf>] <group> <command>...
    turbio.py --help
    
Options:
  -h --help                 This screen
  -c <conf>, --conf=<conf>  Path to config file [default: /etc/turbio.yml]
```
